package main;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import com.sun.javafx.scene.control.skin.ButtonSkin;
import javafx.animation.*;
import javafx.scene.control.*;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebView;
import javafx.scene.web.WebEngine;
import javafx.stage.Stage;
import javafx.scene.shape.Circle;
import javafx.util.Duration;
import gen.lulu2BaseVisitor;
import gen.lulu2Lexer;
import gen.lulu2Parser;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.*;

import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

import java.io.FileInputStream;

import javafx.geometry.Insets;
import javafx.beans.value.ObservableValue;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.reactfx.Subscription;

public class welcome_screen extends Application {


    public boolean theme_toggle=true;
    Error_redirector syntax = new Error_redirector();
    private static final String[] KEYWORDS = new String[]{
            "abstract", "assert", "boolean", "break", "byte",
            "case", "catch", "char", "class", "const",
            "continue", "default", "do", "double", "else",
            "enum", "extends", "final", "finally", "float",
            "for", "goto", "if", "implements", "import",
            "instanceof", "int", "interface", "long", "native",
            "new", "package", "private", "protected", "public",
            "return", "short", "static", "strictfp", "super", "string",
            "switch", "synchronized", "this", "throw", "throws",
            "transient", "try", "void", "volatile", "while", "declare", "function",
            "true", "false", "type", "destruct", "allocate", "bool"
    };

    Map<String,String> parent=new HashMap<String ,String>();
    Map<String,String> vars=new HashMap<String ,String>();
    private static final String KEYWORD_PATTERN = "\\b(" + String.join("|", KEYWORDS) + ")\\b";
    private static final String PAREN_PATTERN = "\\(|\\)";
    private static final String BRACE_PATTERN = "\\{|\\}";
    private static final String BRACKET_PATTERN = "\\[|\\]";
    private static final String SEMICOLON_PATTERN = "\\;";
    private static final String STRING_PATTERN = "\"([^\"\\\\]|\\\\.)*\"";
    private static final String COMMENT_PATTERN = "//[^\n]*" + "|" + "/\\*(.|\\R)*?\\*/";
    private static String ERROR = "";

    private static final Pattern PATTERN = Pattern.compile(
            "(?<KEYWORD>" + KEYWORD_PATTERN + ")"
                    + "|(?<PAREN>" + PAREN_PATTERN + ")"
                    + "|(?<BRACE>" + BRACE_PATTERN + ")"
                    + "|(?<BRACKET>" + BRACKET_PATTERN + ")"
                    + "|(?<SEMICOLON>" + SEMICOLON_PATTERN + ")"
                    + "|(?<STRING>" + STRING_PATTERN + ")"
                    + "|(?<COMMENT>" + COMMENT_PATTERN + ")"
    );

    public static class Error_redirector extends BaseErrorListener {

        public static Map<String, String> lex_error = new <String, String>HashMap();

        public void matchRegExp(WebEngine result, String text)
        {

            result.loadContent("");
            semantics run = new semantics();
            run.run_semantics(text);
            String[] lines = text.split("\r\n|\r|\n");
            ////////////////////////////////////////////////Lexical ERRORS//////////////////////////////////////////////////////////////
            if (!lex_error.isEmpty() && lex_error != null)
            {
                    String Full_content = "";
                        for (int j = 0; j < lines.length; j++)
                        {

                            int count = 0;
                            String[] pos = {"", "", ""};
                            String err_line = "";
                            String err_message = "";
                                for (Map.Entry<String, String> entry : lex_error.entrySet())
                                {
                                     pos = entry.getKey().split(":");
                                        if (j == Integer.parseInt(pos[0]) - 1)
                                        {
                                             count++;
                                             err_line = entry.getKey();
                                             err_message = entry.getValue();

                                        }
                                }
                             if (count == 0 && lines[j] != "")
                             {

                                 String content = String.format("<p style=\"margin:1;\"><span style=\"background-color:rgba(38, 255, 26, 0.85);color:black;padding:3;\">%s</span> %s</p>", Integer.toString(j), lines[j]);
                                 Full_content = Full_content + content;

                             }
                             else
                                 {

                                     int first = 0;
                                     int last = lines[j].length();
                                     int start = Integer.parseInt(err_line.split(":")[1]);
                                     int end = Integer.parseInt(err_line.split(":")[1]) + Integer.parseInt(err_line.split(":")[2]);
                                     String content = String.format("<p style=\"margin:1;\"><span style=\"background-color:red;padding:3;\">%s</span> %s<span style=\"margin:1;background-color:red;cursor:pointer;margin:1;display:inline-block;\" title=\"%s\">%s</span>%s</p>", Integer.toString(j), lines[j].substring(first, start), err_message, lines[j].substring(start, end), lines[j].substring(end, last));
                                     Full_content = Full_content + content;

                                 }

                }


                result.loadContent("<html><body style=\"color : #b4b4b4; background-color : #393939;border : 2;border-radius : 5; border-color : rgba(38, 255, 26, 0.85)\">" + Full_content + "</body></html>");
                lex_error.clear();
            }
            else
                {
                    System.out.println("no lex errors");
                    result.loadContent("<html><body style=\"color : #b4b4b4; background-color : #393939;border : 2;border-radius : 5; border-color : rgba(38, 255, 26, 0.85)\"><h1 style=\"margin-top:35%;font-size:50;margin-left:26%;\">No Errors Found</h1></body></html>");
                    lex_error.clear();
                }

            ////////////////////////////////////////////////SEMANTIC ERRORS//////////////////////////////////////////////////////////////





        }

        @Override
        public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
            try
            {


                    Pattern len_char = Pattern.compile("[0-9]+" + Pattern.quote(":") + "[0-9]+");
                    Matcher matcher = len_char.matcher(offendingSymbol.toString());

                        if (matcher.find())
                        {
                            String p[] = matcher.group(0).split(":");
                            String pos = Integer.toString(line) + ":" + Integer.toString(charPositionInLine) + ":" + Integer.toString(Integer.parseInt(p[1]) - Integer.parseInt(p[0]) + 1);
                            setList(pos, msg);



                        }
            } catch (Exception f)
            {
                    setList("1:0:1", "Unauthorized charachter in code");
            }
        }

        public void setList(String pos, String msg) {
            this.lex_error.put(pos, msg);

        }

    }


    @Override
    public void start(Stage primaryStage) throws FileNotFoundException {

/////////////////////////////////////////////welcome
        BorderPane root1 = new BorderPane();
        VBox start = new VBox();
        Scene scene1 = new Scene(root1, 800, 600);
        Label welcome = new Label();
        welcome.setText("Welcome!");
        welcome.setId("welcome");
        MaterialDesignButton btn_start = new MaterialDesignButton("Let's Start");
        btn_start.setId("btn_start");
        start.setSpacing(150);
        start.getChildren().add(welcome);
        start.getChildren().add(btn_start);
        start.setAlignment(Pos.CENTER);
        root1.setCenter(start);
        primaryStage.setTitle("LULU editor");
        primaryStage.setScene(scene1);
        primaryStage.setResizable(false);
        scene1.getStylesheets().addAll(this.getClass().getResource("welcome.css").toExternalForm());
/////////////////////////////////////////////////////////theme
        BorderPane root2 = new BorderPane();
        Scene scene2 = new Scene(root2, 800, 600);
        /////////////////////
        MaterialDesignButton btn_confirm = new MaterialDesignButton("confirm");
        btn_confirm.setId("btn_confirm");
        ///////////////////////
        Label chose = new Label();
        chose.setText("Choose your theme");
        chose.setId("choose");
        ///////////////////////
        HBox ch = new HBox();
        HBox confirm = new HBox();
        confirm.getChildren().add(btn_confirm);
        ch.getChildren().add(chose);
        ch.setAlignment(Pos.CENTER);
        RadioButton radio_dark = new RadioButton();
        radio_dark.setText("dracula");
        radio_dark.setTextFill(Color.web("#b4b4b4"));
        RadioButton radio_light = new RadioButton();
        radio_light.setText("light");
        radio_light.setTextFill(Color.web("#b4b4b4"));
        ///////////////////////////
        VBox dark = new VBox();
        VBox light = new VBox();
        Image image1 = new Image(new FileInputStream("C:\\Users\\farzam\\Desktop\\LULU_2.1\\src\\main\\Asset 1.png"));
        Image image2 = new Image(new FileInputStream("C:\\Users\\farzam\\Desktop\\LULU_2.1\\src\\main\\Asset 4.png"));
        ImageView imageView1 = new ImageView(image1);
        ImageView imageView2 = new ImageView(image2);
        dark.getChildren().add(imageView1);
        light.getChildren().add(imageView2);
        dark.getChildren().add(radio_dark);
        light.getChildren().add(radio_light);
        root2.setTop(ch);
        BorderPane.setMargin(ch, new Insets(10, 10, 10, 10));
        root2.setLeft(dark);
        root2.setRight(light);
        BorderPane.setMargin(dark, new Insets(70, 10, 10, 10));
        BorderPane.setMargin(light, new Insets(70, 10, 10, 10));
        dark.setSpacing(5);
        light.setSpacing(5);
        root2.setBottom(confirm);
        BorderPane.setMargin(confirm, new Insets(60));
        dark.setAlignment(Pos.CENTER);
        light.setAlignment(Pos.CENTER);
        confirm.setAlignment(Pos.CENTER);
        ////////////////////////////////////////////////////////
        final ToggleGroup group = new ToggleGroup();
        radio_dark.setToggleGroup(group);
        radio_light.setToggleGroup(group);
        radio_dark.setUserData("dark");
        radio_light.setUserData("light");
        ////////////////////////////////////////////////////////editor
        CodeArea codeArea = new CodeArea();
        codeArea.setId("root3");
        codeArea.setStyle(" -fx-highlight-fill: yellow;");
        MaterialDesignButton btn_results = new MaterialDesignButton("Symbol Table");
        MaterialDesignButton btn_open = new MaterialDesignButton("Lunch");
        MaterialDesignButton btn_show_errors = new MaterialDesignButton("Memory Table");
        HBox bt1 = new HBox();
        HBox bt2 = new HBox();
        HBox bt3 = new HBox();
        btn_open.getStyleClass().add("toolbar_btn");
        btn_show_errors.getStyleClass().add("toolbar_btn");
        btn_results.getStyleClass().add("toolbar_btn");
        final Pane leftSpacer = new Pane();
        HBox.setHgrow(
                leftSpacer,
                Priority.SOMETIMES
        );

        final Pane rightSpacer = new Pane();
        HBox.setHgrow(
                rightSpacer,
                Priority.SOMETIMES
        );

        ToolBar toolBar1 = new ToolBar(
                btn_open, leftSpacer, btn_results, rightSpacer, btn_show_errors);
        toolBar1.setId("toolbar");

        ////////////////////codearea

        codeArea.setParagraphGraphicFactory(LineNumberFactory.get(codeArea));
        Subscription cleanupWhenNoLongerNeedIt = codeArea.multiPlainChanges().successionEnds(java.time.Duration.ofMillis(500)).subscribe(ignore -> codeArea.setStyleSpans(0, computeHighlighting(codeArea.getText(), codeArea)));
        BorderPane root3 = new BorderPane(new VirtualizedScrollPane<>(codeArea));
        Scene scene3 = new Scene(root3, 800, 800);
        root3.setTop(toolBar1);
        scene3.getStylesheets().add(this.getClass().getResource("java_keywords").toExternalForm());
        primaryStage.setScene(scene3);
        WebView result = new WebView();
        if(theme_toggle){
            result.setStyle("-fx-background-color: #393939");
        }else {
            result.setStyle("-fx-background-color: #white");
        }
        WebEngine webEngine = result.getEngine();
        BorderPane root4 = new BorderPane();
        MaterialDesignButton btn_back = new MaterialDesignButton("GO Back");
        btn_back.setId("btn_back");
        ToolBar toolbar2 = new ToolBar(btn_back);
        if(theme_toggle){
            toolbar2.setStyle("-fx-background-color: #393939");
        }else{
            toolbar2.setStyle("-fx-background-color: white");
        }

        root4.setTop(toolbar2);
        root4.setCenter(result);
        Scene scene4 = new Scene(root4, 800, 800);

        /////////////////////////////////////////////////////////////////////////GRIDPANE/////////////////////////////////////////////////////
        BorderPane root5=new BorderPane();
        ScrollPane sc=new ScrollPane();
        Scene scene5=new Scene(root5,800,800);
        MaterialDesignButton btn_back_2=new MaterialDesignButton("Go back");
        btn_back_2.setStyle("");
        ToolBar toolbar3=new ToolBar(btn_back_2);
        root5.setTop(toolbar3);
        btn_back_2.setId("btn_back_2");

        GridPane parent_table=new GridPane();
        parent_table.setAlignment(Pos.CENTER);
        sc.setContent(parent_table);
        sc.setFitToWidth(true);
        root5.setCenter(sc);
        if (theme_toggle) {
            toolbar3.setStyle("-fx-background-color: #393939;");
        }else{
            toolbar3.setStyle("-fx-background-color: white;");
        }
        List parents=new ArrayList();
        List childs=new ArrayList();
        parent.put("golbal","myfun");
        parent.put("myfun","if");
        parent.put("golba1l","myfun");
        parent.put("myfun1","if");
        parent.put("golbal2","myfun");
        parent.put("myfun2","if");
        parent.put("golbal2","myfun");
        parent.put("myfun2","if");
        parent.put("golbal3","myfun");
        parent.put("myfun3","if");
        parent.put("golbal4","myfun");
        parent.put("myfun4","if");
        parent.put("golbal5","myfun");
        parent.put("myfun5","if");
        parent.put("golbal6","myfun");
        parent.put("myfun6","if");
        parent.put("golbal7","myfun");
        parent.put("myfun7","if");
        parent_table.setAlignment(Pos.CENTER);
        parent_table.setId("parent");
        if(theme_toggle){
            parent_table.setStyle("-fx-padding: 10;" +
                    "-fx-border-style: solid inside;" +
                    "-fx-border-width: 3;" +
                    "-fx-text-fill: #b4b4b4;" +
                    "-fx-border-color: rgba(38, 255, 26, 0.85);" +
                    "-fx-border-radius: 5px;" +
                    "-fx-font-size: 30;" +
                    "-fx-border-insets: 5;" +
                    "-fx-background-color: #393939;");
        }else {
            parent_table.setStyle("-fx-padding: 10;" +
                    "-fx-border-style: solid inside;" +
                    "-fx-border-width: 3;" +
                    "-fx-text-fill: black;" +
                    "-fx-border-color:#4d4dff;" +
                    "-fx-border-radius: 5px;" +
                    "-fx-font-size: 30;" +
                    "-fx-border-insets: 5;" +
                    "-fx-background-color: white;");
        }
        parent_table.setVgap(10);
        parent_table.setHgap(10);

        parent_table.setPadding(new Insets(25,25,25,25));
        parent_table.setGridLinesVisible(false);
        if(theme_toggle) {
            scene5.getStylesheets().add(this.getClass().getResource("symbol_dark.css").toExternalForm());
        }else {
            scene5.getStylesheets().add(this.getClass().getResource("symbol_light.css").toExternalForm());
        }
        /////////////////////////////////////////////////////

        BorderPane root6=new BorderPane();
        ScrollPane sc2=new ScrollPane();
        Scene scene6=new Scene(root6,800,800);
        MaterialDesignButton btn_back_3=new MaterialDesignButton("Go back");
        //btn_back_2.setStyle("");
        ToolBar toolbar4=new ToolBar(btn_back_3);
        root6.setTop(toolbar4);
        btn_back_3.setId("btn_back_3");

        GridPane sym_table=new GridPane();
        sym_table.setAlignment(Pos.CENTER);

        sc2.setContent(sym_table);
        sc2.setFitToWidth(true);
        root6.setCenter(sc2);
        if(theme_toggle){
            toolbar4.setStyle("-fx-background-color: #393939");
        }else{
            toolbar4.setStyle("-fx-background-color: white");
        }
        vars.put("a","int");
        vars.put("b","float");
        vars.put("a1","int");
        vars.put("b1","float");
        vars.put("a2","int");
        vars.put("b2","float");
        vars.put("a3","int");
        vars.put("b3","float");
        vars.put("a4","int");
        vars.put("b4","float");

        sym_table.setAlignment(Pos.CENTER);
        sym_table.setId("parent");
        if(theme_toggle){
            sym_table.setStyle("-fx-padding: 10;" +
                    "-fx-border-style: solid inside;" +
                    "-fx-border-width: 3;" +
                    "-fx-text-fill: #b4b4b4;" +
                    "-fx-border-color: rgba(38, 255, 26, 0.85);" +
                    "-fx-border-radius: 5px;" +
                    "-fx-font-size: 30;" +
                    "-fx-border-insets: 5;" +
                    "-fx-background-color: #393939;");
        }else{
            sym_table.setStyle("-fx-padding: 10;" +
                    "-fx-border-style: solid inside;" +
                    "-fx-border-width: 3;" +
                    "-fx-text-fill: black;" +
                    "-fx-border-color: #4d4dff;" +
                    "-fx-border-radius: 5px;" +
                    "-fx-font-size: 30;" +
                    "-fx-border-insets: 5;" +
                    "-fx-background-color: white;");
        }
        sym_table.setVgap(10);
        sym_table.setHgap(10);

        sym_table.setPadding(new Insets(25,25,25,25));
        sym_table.setGridLinesVisible(false);
        if (theme_toggle){
            sym_table.getStylesheets().add(this.getClass().getResource("symbol_dark.css").toExternalForm());
        }else{
            sym_table.getStylesheets().add(this.getClass().getResource("symbol_light.css").toExternalForm());
        }

        List name=new ArrayList();
        List types=new ArrayList();

        Label variable_text=new Label("Variable");
        Label type_text=new Label("Type");
        if(theme_toggle){
            variable_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: b4b4b4;");
            type_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: b4b4b4;"

            );
        }else {
            variable_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: black;");
            type_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: black;"

            );
        }
        variable_text.setAlignment(Pos.BASELINE_CENTER);
        type_text.setAlignment(Pos.BASELINE_CENTER);











        if(theme_toggle){
            scene6.getStylesheets().add(this.getClass().getResource("symbol_dark.css").toExternalForm());
        }else{
            scene6.getStylesheets().add(this.getClass().getResource("symbol_light.css").toExternalForm());
        }


        Label parent_text=new Label("Parent");
        Label child_text=new Label("Child");
        if(theme_toggle) {
            parent_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: b4b4b4;");
            child_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: b4b4b4;"

            );
        }else{
            parent_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: black;");
            child_text.setStyle("-fx-font-size: 30;" +
                    "-fx-text-fill: black;");

        }
        parent_table.add(parent_text,0,0);
        parent_table.add(child_text,2,0);
        parent_text.setAlignment(Pos.BASELINE_CENTER);
        child_text.setAlignment(Pos.BASELINE_CENTER);
        sym_table.add(variable_text,0,0);
        sym_table.add(type_text,2,0);
        //////////////////////////////////////////////mmeorry acllocation table goes here////////////////////////////////////////////


        BorderPane root7=new BorderPane();
        ScrollPane sc3=new ScrollPane();
        Scene scene7=new Scene(root7,800,800);
        MaterialDesignButton btn_back_4=new MaterialDesignButton("Go back");
        //btn_back_2.setStyle("");
        ToolBar toolbar5=new ToolBar(btn_back_4);
        root7.setTop(toolbar5);
        btn_back_4.setId("btn_back_4");

        GridPane memory_table=new GridPane();
        memory_table.setAlignment(Pos.CENTER);

        sc3.setContent(memory_table);
        sc3.setFitToWidth(true);
        root7.setCenter(sc3);

        btn_back_3.setOnAction(event -> {

            primaryStage.setScene(scene5);

        });
        btn_results.setOnAction(event -> {

            int row=1;
            for(Map.Entry<String ,String>entry:parent.entrySet()){
                parents.add(entry.getValue());
                childs.add(entry.getKey());
            }
            for (int i=0;i<parents.size();i++){
                Label labels_p=new Label(childs.get(i).toString());
                Label labels_c=new Label(parents.get(i).toString());
                Label dir=new Label("-->");
                parent_table.add(labels_p,0,row);
                if(theme_toggle){
                    labels_c.setTextFill(Color.web("#b4b4b4"));
                    labels_p.setTextFill(Color.web("#b4b4b4"));
                    dir.setTextFill(Color.web("#b4b4b4"));
                }else{
                    labels_c.setTextFill(Color.BLACK);
                    labels_p.setTextFill(Color.BLACK);
                    dir.setTextFill(Color.BLACK);
                }
                labels_c.getStyleClass().add("scopes");
                labels_p.getStyleClass().add("scopes");
                labels_c.setMinWidth(100);
                labels_p.setMinWidth(100);
                labels_c.setAlignment(Pos.BASELINE_CENTER);
                labels_p.setAlignment(Pos.BASELINE_CENTER);



                //////////////////////////////////////
                labels_c.setOnMouseClicked(event2 -> {
                    primaryStage.setScene(scene6);
                    System.out.println(labels_c.getText());
                    /////////////////search in scopes
                    int row2=1;

                    for(Map.Entry<String ,String>entry:vars.entrySet()){
                        name.add(entry.getKey());
                        types.add(entry.getValue());
                    }
                    for (int k=0;k<name.size();k++){
                        Label n=new Label(name.get(k).toString());
                        Label t=new Label(types.get(k).toString());
                        Label dot=new Label(":");
                        sym_table.add(n,0,row2);
                        sym_table.add(dot,1,row2);
                        sym_table.add(t,2,row2);
                        if(theme_toggle){
                            n.setTextFill(Color.web("#b4b4b4"));
                            t.setTextFill(Color.web("#b4b4b4"));
                            dot.setTextFill(Color.web("#b4b4b4"));
                        }else {
                            n.setTextFill(Color.BLACK);
                            t.setTextFill(Color.BLACK);
                            dot.setTextFill(Color.BLACK);
                        }
                        n.getStyleClass().add("sym");
                        t.getStyleClass().add("sym");
                        n.setMinWidth(100);
                        t.setMinWidth(100);
                        n.setAlignment(Pos.BASELINE_CENTER);
                        t.setAlignment(Pos.BASELINE_CENTER);
                        row2++;
                    }

                });
                labels_p.setOnMouseClicked(event3 -> {
                    primaryStage.setScene(scene6);
                    System.out.println(labels_p.getText());
                    int row2=1;

                    for(Map.Entry<String ,String>entry:vars.entrySet()){
                        name.add(entry.getKey());
                        types.add(entry.getValue());
                    }
                    for (int k=0;k<name.size();k++){
                        Label n=new Label(name.get(k).toString());
                        Label t=new Label(types.get(k).toString());
                        Label dot=new Label(":");
                        sym_table.add(n,0,row2);
                        sym_table.add(dot,1,row2);
                        sym_table.add(t,2,row2);
                       if(theme_toggle){
                           n.setTextFill(Color.web("#b4b4b4"));
                           t.setTextFill(Color.web("#b4b4b4"));
                           dot.setTextFill(Color.web("#b4b4b4"));
                       }else{
                           n.setTextFill(Color.BLACK);
                           t.setTextFill(Color.BLACK);
                           dot.setTextFill(Color.BLACK);
                       }
                        n.getStyleClass().add("sym");
                        t.getStyleClass().add("sym");
                        n.setMinWidth(100);
                        t.setMinWidth(100);
                        n.setAlignment(Pos.BASELINE_CENTER);
                        t.setAlignment(Pos.BASELINE_CENTER);
                        row2++;
                    }

                });
                parent_table.add(dir,1,row);
                parent_table.add(labels_c,2,row);
                row++;

            }


            primaryStage.setScene(scene5);

        });
///////////////////////////////////////first sym_table///////////////////////////////////
        btn_back_2.setOnAction(event -> {


            primaryStage.setScene(scene3);
            if(theme_toggle){
                scene5.getStylesheets().addAll(this.getClass().getResource("dark_editor.css").toExternalForm());
            }else{
                scene5.getStylesheets().addAll(this.getClass().getResource("light_editor.css").toExternalForm());
            }

        });



        ///////////////////////////////////////////lunch/////////////////////////////////////////////////////
        btn_open.setOnAction(event -> {

            syntax.matchRegExp(webEngine, codeArea.getText());
            primaryStage.setScene(scene4);
            if(theme_toggle){
                scene4.getStylesheets().addAll(this.getClass().getResource("dark.css").toExternalForm());
            }else {
                scene4.getStylesheets().addAll(this.getClass().getResource("light.css").toExternalForm());
            }
        });
        btn_back.setOnAction(event -> {


            primaryStage.setScene(scene3);
        });
        btn_confirm.setOnAction(event -> {


            primaryStage.setScene(scene3);
        });


        //////////////////////////radio buttons
        group.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> ov, Toggle old_t, Toggle new_t) -> {
                    if (group.getSelectedToggle().toString() != null) {
                        if (group.getSelectedToggle().getUserData().toString() == "dark") {
                            chose.setStyle("-fx-text-fill: #b4b4b4; -fx-font-size: 60px;-fx-font-family: Georgia;");
                            //chose.setTextFill(Color.web("#b4b4b4"));
                            root2.setStyle("-fx-background-color: #393939;");
                            String hover = "-fx-border-width: 3px;-fx-border-color: rgba(38, 255, 26, 0.85);-fx-border-radius: 5px;-fx-background-color:rgba(38, 255, 26, 0.85);-fx-text-fill: #393939;-fx-font-size: 30px;";
                            String idele = "-fx-border-width: 3px;-fx-border-color: rgba(38, 255, 26, 0.85);-fx-border-radius: 5px;-fx-background-color:transparent;-fx-text-fill: #b4b4b4;-fx-font-size: 30px;";
                            btn_confirm.setStyle(idele);
                            btn_confirm.setOnMouseEntered(event -> btn_confirm.setStyle(hover));
                            btn_confirm.setOnMouseExited(event -> btn_confirm.setStyle(idele));
                            radio_dark.setTextFill(Color.web("#b4b4b4"));
                            radio_light.setTextFill(Color.web("#b4b4b4"));
                            //System.out.println(1);

                        }
                        if (group.getSelectedToggle().getUserData().toString() == "light") {
                            theme_toggle=false;
                            chose.setStyle("-fx-text-fill: #000000; -fx-font-size: 60px;-fx-font-family: Georgia;");
                            //chose.setTextFill(Color.web("#000000"));
                            root2.setStyle("-fx-background-color: #ffffff;");
                            String hover = "-fx-border-width: 3px;-fx-border-color: #4d4dff;-fx-border-radius: 5px;-fx-background-color:#4d4dff;-fx-text-fill: #ffffff;-fx-font-size: 30px;";
                            String idele = "-fx-border-width: 3px;-fx-border-color: #4d4dff;-fx-border-radius: 5px;-fx-background-color:transparent;-fx-text-fill: #000000;-fx-font-size: 30px;";
                            btn_confirm.setStyle(idele);
                            btn_confirm.setOnMouseEntered(event -> btn_confirm.setStyle(hover));
                            btn_confirm.setOnMouseExited(event -> btn_confirm.setStyle(idele));
                            radio_dark.setTextFill(Color.web("#000000"));
                            radio_light.setTextFill(Color.web("#000000"));
                            //System.out.println(0);
                        }

                    }
                }
        );

        root2.setId("root2");
        primaryStage.show();
        btn_start.setOnAction((event) -> {
            scene2.getStylesheets().addAll(this.getClass().getResource("dark.css").toExternalForm());
            primaryStage.setScene(scene2);
            radio_dark.setSelected(true);
        });


    }

    public static StyleSpans<Collection<String>> computeHighlighting(String text, CodeArea code) {
        //match with pattern and return as a stringbuilder
        Matcher matcher = PATTERN.matcher(text);
        int lastKwEnd = 0;
        StyleSpansBuilder<Collection<String>> spansBuilder
                = new StyleSpansBuilder<>();
        while (matcher.find()) {
            String styleClass =
                    matcher.group("KEYWORD") != null ? "keyword" :
                            matcher.group("PAREN") != null ? "paren" :
                                    matcher.group("BRACE") != null ? "brace" :
                                            matcher.group("BRACKET") != null ? "bracket" :
                                                    matcher.group("SEMICOLON") != null ? "semicolon" :
                                                            matcher.group("STRING") != null ? "string" :
                                                                    matcher.group("COMMENT") != null ? "comment" :
                                                                            null; /* never happens */
            assert styleClass != null;
            spansBuilder.add(Collections.emptyList(), matcher.start() - lastKwEnd);
            spansBuilder.add(Collections.singleton(styleClass), matcher.end() - matcher.start());
            lastKwEnd = matcher.end();
        }
        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        /////////////////////////////////Semnatics go here


        return spansBuilder.create();
    }


    public static void main(String[] args) throws FileNotFoundException {
        launch(args);


    }


}
class MaterialDesignButton extends Button {
    private Circle circleRipple;
    private Rectangle rippleClip = new Rectangle();
    private Duration rippleDuration = Duration.millis(250);
    private double lastRippleHeight = 0;
    private double lastRippleWidth = 0;
    private Color rippleColor = new Color(0, 0, 0, 0.11);

    public MaterialDesignButton(String text) {
        super(text);
        getStyleClass().addAll("md-button");
        createRippleEffect();
    }

    @Override
    protected Skin<?> createDefaultSkin() {
        final ButtonSkin buttonSkin = new ButtonSkin(this);
        // Adding circleRipple as fist node of button nodes to be on the bottom
        this.getChildren().add(0, circleRipple);
        return buttonSkin;
    }

    private void createRippleEffect() {
        circleRipple = new Circle(0.1, rippleColor);
        circleRipple.setOpacity(0.0);
        // Optional box blur on ripple - smoother ripple effect
        //circleRipple.setEffect(new BoxBlur(3, 3, 2));
        // Fade effect bit longer to show edges on the end of animation
        final FadeTransition fadeTransition = new FadeTransition(rippleDuration, circleRipple);
        fadeTransition.setInterpolator(Interpolator.EASE_OUT);
        fadeTransition.setFromValue(1.0);
        fadeTransition.setToValue(0.0);
        final Timeline scaleRippleTimeline = new Timeline();
        final SequentialTransition parallelTransition = new SequentialTransition();
        parallelTransition.getChildren().addAll(
                scaleRippleTimeline,
                fadeTransition
        );
        // When ripple transition is finished then reset circleRipple to starting point
        parallelTransition.setOnFinished(event -> {
            circleRipple.setOpacity(0.0);
            circleRipple.setRadius(0.1);
        });
        this.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            parallelTransition.stop();
            // Manually fire finish event
            parallelTransition.getOnFinished().handle(null);
            circleRipple.setCenterX(event.getX());
            circleRipple.setCenterY(event.getY());
            // Recalculate ripple size if size of button from last time was changed
            if (getWidth() != lastRippleWidth || getHeight() != lastRippleHeight) {
                lastRippleWidth = getWidth();
                lastRippleHeight = getHeight();
                rippleClip.setWidth(lastRippleWidth);
                rippleClip.setHeight(lastRippleHeight);
                // try block because of possible null of Background, fills ...
                try {
                    rippleClip.setArcHeight(this.getBackground().getFills().get(0).getRadii().getTopLeftHorizontalRadius());
                    rippleClip.setArcWidth(this.getBackground().getFills().get(0).getRadii().getTopLeftHorizontalRadius());
                    circleRipple.setClip(rippleClip);
                } catch (Exception e) {
                }
                // Getting 45% of longest button's length, because we want edge of ripple effect always visible
                double circleRippleRadius = Math.max(getHeight(), getWidth()) * 0.45;
                final KeyValue keyValue = new KeyValue(circleRipple.radiusProperty(), circleRippleRadius, Interpolator.EASE_OUT);
                final KeyFrame keyFrame = new KeyFrame(rippleDuration, keyValue);
                scaleRippleTimeline.getKeyFrames().clear();
                scaleRippleTimeline.getKeyFrames().add(keyFrame);
            }
            parallelTransition.playFromStart();
        });
    }

    public void setRippleColor(Color color) {
        circleRipple.setFill(color);
    }
}
class semantics {
    public void run_semantics(String s) {
        CharStream input = CharStreams.fromString(s);
        lulu2Lexer lexer = new lulu2Lexer(input);
        lexer.removeErrorListeners();
        lexer.addErrorListener(new welcome_screen.Error_redirector());
        lexer.setTokenFactory(new CommonTokenFactory(true));
        TokenStream tokens = new UnbufferedTokenStream<CommonToken>(lexer);
        lulu2Parser parser = new lulu2Parser(tokens);
        //parser.removeErrorListeners();
        parser.addErrorListener(new welcome_screen.Error_redirector());
        parser.setBuildParseTree(true);
        ParseTree tree = parser.program();

        System.out.println(tree.toStringTree(parser));
        lulu2BaseVisitor loader = new lulu2BaseVisitor();
        loader.visit(tree);
    }


}


