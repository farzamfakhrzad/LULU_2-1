grammar lulu2;
WS : [ \r\t\n]+ -> skip  ;
Multiline_Comment:'%~'.*?'~%' -> skip;
Oneline_Comment:'%%' .*? '\r'? '\n' -> skip;
program:ft_dcl?ft_def+;
ft_dcl:'declare''{'(func_dcl|type_decl|var_def)+'}';
func_dcl:('('args')''=')?ID'('(args|args_var)?')'';';
args:types('['']')* |args','types ('['']')*;
args_var:types('['']')* ID|args_var','types('['']')*ID;
type_decl:ID';';
var_def:'const'?types var_val(','var_val)*';';
var_val:ref('='expr)?;
ft_def:(type_def|fun_def);
type_def:'type'ID(':' ID)?'{'component+'}';
component:Access_modifier?(var_def|fun_def);
Access_modifier:'private'|'public'|'protected';
fun_def:('('args_var')''=')? 'function' ID'('args_var?')' block;
block:'{'(var_def|stmt)*'}';
stmt:assign';'|func_call';'|cond_stmt|loop_stmt|'return' ';'|
'break'';'|'continue'';'|'destruct'('['']')* ID';';
assign:(var|'('var (','var)*')')'='expr;
var:(('this'|'super')'.')?ref ('.' ref)*;
ref:ID ('['expr']')*;
expr:'('expr')'  |('-'|'!'|'~')expr|expr  ('*'|'/'|'%') expr|expr  ('+'|'-') expr|expr ('>'|'<'|'<='|'>=') expr |expr ('=='|'!=')expr|expr binary_op expr|expr('||'|'&&') expr|result |'allocate'handle_call |func_call |var  |list |'nil' ;
func_call:(var'.')?handle_call|'read''('var')'|'write' '('var')';
list:'['(expr|list)(','(expr|list))*']';
handle_call:ID'(' params?')';
params:expr|expr','params;
cond_stmt:'if' expr block('else'block)?|'switch'var'{'('case' (Digit|HEX)':'block)*
'default'':'block'}';
loop_stmt:'for'(types?assign)?';'expr';'assign?block|'while'expr block;
//Unary_op:'-'|'!'|'~';
binary_op:Bitwise;
Bitwise :  '&' | '|' | '^';
result:String|HEX|Digit res_follow? |Bool;
//float  grammer
res_follow:'.'Digit fl?;
fl: Exponent Digit  ;
Exponent : 'e+'|'e-';
Bool:'true'|'false';
types:'int'|'string'|'bool'|'float'|ID;
//id grammer
ID:[a-zA-Z_#]+[a-zA-Z_#0-9]*;
Digit:[0-9]+ ;
// hex grammer
 HEX:  HT ('0'..'9'|'a'..'f'|'A'..'F')+;
 HT:'0x'|'0X';
 // string grammer
 String : '\'' (~('\\')|ESC)*? '\'' ;
 fragment ESC : '\\b'|'\\n'|'\\r'|'\\t'|'\\\\'|'\\0'|'\\\''|'\\x'[0-9a-fA-F][0-9a-fA-F] |'\\X'[0-9a-fA-F][0-9a-fA-F];

